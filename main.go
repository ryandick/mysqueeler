package main

import (
	"database/sql"
	"fmt"
	"os"
	"os/exec"
	"time"

	_ "github.com/Go-SQL-Driver/MySQL"
)

const (
	DB_HOST   = "tcp(127.0.0.1:3306)"
	DB_NAME   = "sipmon"
	DB_USER   = "root"
	DB_PASS   = ""
	TICK_SPAN = 10 * time.Second
)

func startMysql() {

	args := os.Args[2:]
	cmd := exec.Command(os.Args[1], args...)
	stdout, err := cmd.Output()

	if err != nil {
		fmt.Println( string(stdout))
		fmt.Println( err.Error())
		return
	}

}

func main() {
	dsn := DB_USER + ":" + DB_PASS + "@" + DB_HOST + "/" + DB_NAME + "?charset=utf8"
	c := time.Tick(TICK_SPAN)
	fmt.Println("testing", DB_HOST + "/" + DB_NAME, "every", TICK_SPAN)
	for now := range c {
		db, err := sql.Open("mysql", dsn)

		var str string
		q := "SELECT 1"
	err = db.QueryRow(q).Scan(&str)
		if err != nil {
			fmt.Println(now, err)
			startMysql()
		} else {
			fmt.Println(now, "mysql is ok")
		}

		db.Close()
	}
}
